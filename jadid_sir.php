<?php
 include_once "vendor/autoload.php";
 use Pondit\Operator\Sum;
 use Pondit\Operator\Subtraction;
 use Pondit\Operator\Division;
 use Pondit\Operator\Multiplication;
 use Pondit\Operator\Modulus;
 use Pondit\Formatter\Formatter;

$sum1=new Sum("1253");
$formatter1=new Formatter();
$formatter1->formatH1($sum1->serialNumber);
$subtraction=new Subtraction("1548");
$formatter1->formatpre($subtraction->serialNumber);

//add 2 and 5


$result=$sum1->add(2,5);
 //display the result using H1
 $formatter1->formatH1($result);

 // Sub 15 and 5
$result=$subtraction->subtraction(15,5);
//display the result using pre
$formatter1->formatPre($result);

// Calling new object/Create new object
$multiplication=new Multiplication("1000010ppp");
$formatter1->formatPre($multiplication->serialNumber);
// Multiplication 5 and 5
$result=$multiplication->multi(5,5);
// display the result using h1
$formatter1->formatH1($result);


// Calling new object/Create new object
$division=new Division("df12grr");
//dispaly serial Number using formatter h1
$formatter1->formatH1($division->serialNumber);
//dispaly division 25 and 5
$result=$division->div(25,5);
//dispaly the result using pre
$formatter1->formatPre($result);

//Calling new object /Create new object
$modulus=new Modulus("mkj12455");
//Dispaly serialNumber using formatter h2
$formatter1->formatH1($modulus->serialNumber);
//Dispaly Modulus 25 and 4
$result=$modulus->modulus(25,4);
$formatter1->formatPre($result);













