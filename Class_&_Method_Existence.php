<?php
// Magic Methods

echo "<h1><center>Class and Mehtod Existence</center></h1>";
class Student{
    public function describe(){
        echo "Students class exist";
    }
}
  // class exist ki na tha check kora
/*
if(class_exists("Students")){
    $student = new Student();
    $student->describe();

}else{
    echo "This is no class";
}
*/

// Method exist ki na tha check kora
if(class_exists("Student")){
    $student = new Student();
    if(method_exists($student,"describes")) {
        $student->describe();
    }else{
        echo "Method Not Found";
    }

}else{
    echo "This is no class";
}