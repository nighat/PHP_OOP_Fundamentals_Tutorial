<?php

include_once "vendor/autoload.php";
use Pondit\Rectangle\Rectangle;
use Pondit\Formatter\Formatter;


$rectangle1= new Rectangle(100,300);
$area1 =$rectangle1->getArea();

$rectangle2 = new Rectangle(89,9);
$area2 = $rectangle2->getArea();

$format= new Formatter();
$format->formatPre($area1);
$format->formatH1($area2);