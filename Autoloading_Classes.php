<?php

include_once "vendor/autoload.php";
use Pondit\Classes\Java;
use Pondit\Classes\Ruby;
use Pondit\Classes\Php;
use Pondit\Classes\Teacher;

$ruby = new Ruby;
$java = new Java;
$php = new Php;
$teacher = new Teacher;