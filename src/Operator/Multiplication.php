<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/17/2018
 * Time: 4:06 PM
 */

namespace Pondit\Operator;


class Multiplication
{
    public $serialNumber=null;
    public function __construct($serialNumber)
    {
        $this->serialNumber="Multiplication:-".$serialNumber;
    }
    public function multi($number1,$number2){

        $result=$number1 *$number2;
        return $result;
    }

}