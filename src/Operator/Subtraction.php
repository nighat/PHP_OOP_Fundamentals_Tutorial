<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/17/2018
 * Time: 4:05 PM
 */

namespace Pondit\Operator;


class Subtraction
{
    public $serialNumber=null;

    public function __construct($serialNumber)
    {
        $this->serialNumber="Sub:-".$serialNumber;
    }

    public function subtraction($number1,$number2){
        $result = $number1-$number2;
        return $result;

    }
}