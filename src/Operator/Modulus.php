<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/17/2018
 * Time: 4:08 PM
 */

namespace Pondit\Operator;


class Modulus
{
    public $serialNumber=null;
    public function __construct($serialNumber)
    {
        $this->serialNumber="Modulus:-".$serialNumber;
    }

    public function modulus($number1,$number2){
        $result=$number1%$number2;
        return $result;
    }
}