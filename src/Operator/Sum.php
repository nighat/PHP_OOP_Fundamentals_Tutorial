<?php

namespace Pondit\Operator;
class Sum
{
    public $serialNumber=null;

    public function __construct($serialNumber)
    {
        $this->serialNumber="Sum:-".$serialNumber;
    }
    //declaration /defination of a method
    public function add($number1,$number2){
        $result = $number1+$number2;
        return $result;
    }
}