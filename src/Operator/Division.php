<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/17/2018
 * Time: 4:07 PM
 */

namespace Pondit\Operator;


class Division
{
    public $serialNumber=null;

    public function __construct($serialNumber)
    {
        $this->serialNumber="Division-".$serialNumber;
    }

    public function div($number1,$number2){
        $result=$number1/$number2;
        return $result;
    }
}