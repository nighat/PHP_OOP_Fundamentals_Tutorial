<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/17/2018
 * Time: 4:24 PM
 */

namespace Pondit\Rectangle;


class Rectangle
{
    public $width;
    public $length;

    public function __construct($width,$length)
    {
        $this->width=$width;
        $this->length=$length;

    }
    public function getArea(){

        $area =$this->width*$this->length;
        return $area;

    }

}