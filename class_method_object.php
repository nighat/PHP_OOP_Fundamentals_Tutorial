<?php

// Normal class thaka method and object create kora
class Person{
    public $id;
    public $name="nighat";
    public $age="23";

    public function __construct($id)
    {
        echo "This is my ID:".$this->id=$id."<br/>";
    }

    public function name(){
        echo "This is my name:".$this->name."<br/>";
    }
    public function age(){
        echo "This is my age:".$this->age."<br/>";
    }

}

$person = new Person("*7655");
$person->name();
$person->age();


//Argument pass kora class thaka method and object create kora

class Person2{
    public $slNumber;
    public $name;
    public $age;

    public function __construct($slNumber)
    {
        echo "This is my Serial Number:".$this->slNumber=$slNumber."<br/>";
    }

    public function name($name){
        echo "This is my name:".$this->name=$name."<br/>";
    }
    public function age($age){
        echo "This is my age:".$this->age=$age."<br/>";

    }

}
$person2 = new Person2("$%098");
$person2->name("Shamiha Benty Shamim");
$person2->age("06");
